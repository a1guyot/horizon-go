package horizon

import (
	"gitlab.com/evertrust/horizon-go/http"
	"gitlab.com/evertrust/horizon-go/license"
	"gitlab.com/evertrust/horizon-go/requests"
	"gitlab.com/evertrust/horizon-go/rfc5280"
	"net/url"
)

type Horizon struct {
	Http     *http.Client
	Requests *requests.Client
	License  *license.Client
	Rfc5280  *rfc5280.Client
}

func (client *Horizon) Init(baseUrl url.URL, apiId string, apiKey string) {
	client.Http = &http.Client{}
	client.Http.Init(baseUrl, apiId, apiKey)
	client.Requests = &requests.Client{Http: client.Http}
	client.License = &license.Client{Http: client.Http}
	client.Rfc5280 = &rfc5280.Client{Http: client.Http}
}
