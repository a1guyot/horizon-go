package rfc5280

type SubjectAlternateName struct {
	SanType string `json:"sanType"`
	Value   string `json:"value"`
}

type CFDistinguishedName struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type CFCertificationRequest struct {
	Dn         string                 `json:"dn"`
	Sans       []SubjectAlternateName `json:"sans"`
	DnElements []CFDistinguishedName  `json:"dnElements"`
	KeyType    string                 `json:"keyType"`
	Pem        string                 `json:"pem"`
}
