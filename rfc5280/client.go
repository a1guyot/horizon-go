package rfc5280

import (
	"gitlab.com/evertrust/horizon-go/http"
	baseHttp "net/http"
	"net/url"
)

type Client struct {
	Http *http.Client
}

func (c *Client) Pkcs10(pkcs10 []byte) (*CFCertificationRequest, error) {
	baseUrl := c.Http.BaseUrl()
	encodedCsr := url.PathEscape(string(pkcs10))
	reqUrl := baseUrl.String() + "/api/v1/rfc5280/pkcs10/" + encodedCsr
	req, err := baseHttp.NewRequest("GET", reqUrl, nil)
	if err != nil {
		return nil, err
	}
	baseResponse, err := c.Http.Do(req)
	if err != nil {
		return nil, err
	}

	response, err := c.Http.Unmarshal(baseResponse)
	if err != nil {
		return nil, err
	}

	var csr CFCertificationRequest
	response.Json().Decode(&csr)
	return &csr, nil
}
