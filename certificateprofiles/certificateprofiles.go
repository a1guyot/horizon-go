package certificateprofiles

type CertificateProfileCryptoPolicy struct {
	Centralized   bool `json:"centralized"`
	Decentralized bool `json:"decentralized"`
	Escrow        bool `json:"escrow"`
}
