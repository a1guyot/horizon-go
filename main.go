package horizon

import (
	"net/url"
)

func main() {
	client := Horizon{}
	client.Init(
		url.URL{
			Scheme: "http",
			Host:   "localhost:9000",
		},
		"administrator",
		"horizon",
	)

	//// Get license information
	//license, err := client.License.Get()
	//if err != nil {
	//	print(err.Error())
	//} else {
	//	print(license)
	//}

	//// Revoke a certificate
	//resp, err := client.Requests.Revoke(
	//	"-----BEGIN CERTIFICATE-----\nMIIEbzCCAlegAwIBAgIIUDXqaVWlcekwDQYJKoZIhvcNAQELBQAwQzELMAkGA1UE\nBhMCRlIxEjAQBgNVBAoTCUV2ZXJUcnVzdDEgMB4GA1UEAxMXRXZlclRydXN0IFFB\nIElzc3VpbmcgQ0EwHhcNMjExMjI0MTUzOTM5WhcNMjIxMjI0MTUzOTM5WjAWMRQw\nEgYDVQQDDAtleGFtcGxlLm9yZzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC\nggEBALGtRX4K5VygDm/mVm4xtmWbMj1CHYMulRhx3f2YDM24mpROLI7r1QYFaO8G\n5aDR/EnqfkP/u9ZE+GwfD7DUusYxUAoJz+x+YsOme8N4SXd17sqUukNrcDSqHiXK\n4vvjp2F5MzRIdWpP0D8tF7L4RfzSOWFlTwSyBIRbYveAdk27wKXkKXVoX+sfaw1b\nvnajEr0k3cvZY2iPNpzgMkzdfHQEvR5MjMoUbLNbSNGtzhi2WrFStEwLfrvM+k4f\nE3krVJ5ezgJzHam2Za3i22yGNwr1c/dWH2dtei6z8u94QMHl2ATBIrSe+Ms6smZW\n51yDqWBcE9TRv1KyAz0tNXKJrg8CAwEAAaOBkzCBkDAMBgNVHRMBAf8EAjAAMB0G\nA1UdDgQWBBQdwuQZUj6nmv1c8EsI/AMbby/o0zAfBgNVHSMEGDAWgBQUENywMwn3\noBqaymoI1hZXciXAqTAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUH\nAwIGCCsGAQUFBwMBMBEGA1UdIAQKMAgwBgYEVR0gADANBgkqhkiG9w0BAQsFAAOC\nAgEAfQV6gfEUj2k1vR1/PpDsnV6Ut1J/jhqwt9A3OpJSpeijqFzFHa0IzWp6DkmJ\nNT6NJFVGwUy9vEwJzDWbTseM0zXgYmOAldqrHpSKjP8EKcWpSKYIklbypJbrjwsR\nllKK6b5iAGLGxvLbRlxBRx4+685EQsgJUN2WRxlhQXFngxxH7c0EosrJOQrFb6yz\n++TShRZmXp/Bep7/ijK1G5VvlmrNmO+CtCKotHHjJwlBujPbPOjCnJur9w2B43ya\nzDwTGTnA9ZLuDqT7aZTI2oln9MYv/EvKAIGJSzUHCtUUEWsUj8CNDepz+Uc4/sqW\nZkfee66+khyXXhO7dw7UcGCNPTmDLmsUDmy7pHKEeo4aFOLQJgA9KCc52x9fvJ/C\nW308YztEpfHgbRm01kSUghDQtMxJVpYsAXyegD9gTIqkpH8/D8bTcdopF3hSOyW/\nKJAQKPKLzjLIruiXGW50GrLjuZ94HmR1T7IeMTE9Wzs6L4Hoht595IENm5mAK6Qn\nVf1MtskxlBSxrr8pIRvTHCUX3t26h/oxbrAAObw2VdMux9JMSGSyAxxIV2sPPfYb\nfQhWxr7wDpsYR6XfH9biaC9mqdQAsYzZ/QnxhrAOi5SSDJtjVJ7skUpdXsgF7/CL\nJGJvg1ctHqQCPt1H7UwMpZHFQIzYAs0b92/oCFp0e2aVS3w=\n-----END CERTIFICATE-----",
	//	certificates.RevocationReasonSuperseded,
	//)
	//if err != nil {
	//	print(err.Error())
	//} else {
	//	print(resp)
	//}

	request, err := client.Requests.Get("61e81c6a2f00003600395f51")
	if err != nil {
		print(err.Error())
	} else {
		print(request)
	}

}
